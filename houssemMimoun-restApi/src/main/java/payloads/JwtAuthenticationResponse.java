package payloads;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.vaibhav.sec.security.UserPrincipal;

public class JwtAuthenticationResponse {

	private String accessToken;
    private String tokenType = "Bearer";
    private UserPrincipal username ;
    private Collection<? extends GrantedAuthority> authorities;
/*
    public JwtAuthenticationResponse(String accessToken) {
        this.accessToken = accessToken;
    }
    */
    public JwtAuthenticationResponse(String accessToken, UserPrincipal username, Collection<? extends GrantedAuthority> authorities) {
        this.accessToken = accessToken;
        this.username = username;
        this.authorities = authorities;
    } 
    
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

	public UserPrincipal getUsername() {
		return username;
	}

	public void UserDetails(UserPrincipal username) {
		this.username = username;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

    
    
    
}
