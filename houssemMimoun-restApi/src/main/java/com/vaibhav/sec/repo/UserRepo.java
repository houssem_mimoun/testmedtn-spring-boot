package com.vaibhav.sec.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vaibhav.sec.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
	
	Optional<User> findById(int id);

	Optional<User> findByEmail(String email);

	Optional<User> findByUsernameOrEmail(String username, String email);

	List<User> findByIdIn(List<Long> userIds);

	Optional<User> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
	
	@Modifying
	@Query("UPDATE User u  SET u.email= :email, u.username= :username, u.password= :password  WHERE u.id = :id")
	int updProfile(@Param("id") Long id, @Param("email") String email, @Param("username") String username, @Param("password") String password);
}
