package com.vaibhav.sec.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vaibhav.sec.model.Question;


@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

}
