package com.vaibhav.sec.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "queestions")
public class Question {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "titre")
	private String titre;
	
	@Column(name = "contenu")
	private String contenu;
	
	@Column(name = "document")
	private String document;
	
	@Column(name = "categorie")
	private String categorie;
	
	@Column(name = "specialite")
	private String specialite;
	
	

	public Question() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Question(String titre, String contenu, String document, String categorie, String specialite) {
		super();
		this.titre = titre;
		this.contenu = contenu;
		this.document = document;
		this.categorie = categorie;
		this.specialite = specialite;
	}

	public Question(long id, String titre, String contenu, String document, String categorie, String specialite) {
		super();
		this.id = id;
		this.titre = titre;
		this.contenu = contenu;
		this.document = document;
		this.categorie = categorie;
		this.specialite = specialite;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}
	
	
}
