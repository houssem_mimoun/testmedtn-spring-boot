package com.vaibhav.sec.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vaibhav.sec.exception.AppException;
import com.vaibhav.sec.exception.ResourceNotFoundException;
import com.vaibhav.sec.model.Question;
import com.vaibhav.sec.repo.QuestionRepository;

import javax.xml.validation.Validator;;

@RestController
@RequestMapping("/api/auth/")
public class QuestionController {
	
	@Autowired
	private QuestionRepository questionRepository; 
	
	@GetMapping("questions")
	public List<Question> getQuestions(){
		return this.questionRepository.findAll();
	}
	
	@GetMapping("questions/{id}")
	public ResponseEntity<Question> getQuestionById(@PathVariable(value = "id") Long id) {
		Question q = questionRepository.findById(id).orElseThrow(() -> new AppException("Question not set."));
		return ResponseEntity.ok().body(q);
	}
	
	@PostMapping("questions")
	public Question createQuestion(@RequestBody Question question) {
		return this.questionRepository.save(question);
	}
	
	@PutMapping("questions/{id}")
	public ResponseEntity<Question> updateQuestion(@PathVariable(value = "id") Long id, 
			@Validated @RequestBody Question questionDetails) 
			throws ResourceNotFoundException{
		Question q = questionRepository.findById(id).orElseThrow(() -> new AppException("Question not set."));
		q.setTitre(questionDetails.getTitre());
		q.setContenu(questionDetails.getContenu());
		q.setDocument(questionDetails.getDocument());
		q.setCategorie(questionDetails.getCategorie());
		q.setSpecialite(questionDetails.getSpecialite());
		return ResponseEntity.ok(this.questionRepository.save(q));
	}
	
	@DeleteMapping("questions/{id}")
	public Map<String, Boolean> deleteQuestion(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		Question q = questionRepository.findById(id).orElseThrow(() -> new AppException("Question not set."));
		this.questionRepository.delete(q);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
