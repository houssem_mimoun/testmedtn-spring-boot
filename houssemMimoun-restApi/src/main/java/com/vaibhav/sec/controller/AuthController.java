package com.vaibhav.sec.controller;

import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.condition.CompositeRequestCondition;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vaibhav.sec.exception.AppException;
import com.vaibhav.sec.model.Role;
import com.vaibhav.sec.model.RoleName;
import com.vaibhav.sec.model.User;
import com.vaibhav.sec.repo.RoleRepo;
import com.vaibhav.sec.repo.UserRepo;
import com.vaibhav.sec.security.JwtTokenProvider;
import com.vaibhav.sec.security.UserPrincipal;

import payloads.ApiResponse;
import payloads.JwtAuthenticationResponse;
import payloads.Login2Request;
import payloads.LoginRequest;
import payloads.RoleRequest;
import payloads.SignUpRequest;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepo userRepository;
	

	@Autowired
	RoleRepo roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	JwtTokenProvider tokenProvider;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		String jwt = tokenProvider.generateToken(authentication);
		UserPrincipal userDetails = (UserPrincipal) authentication.getPrincipal();
		//long jwtDetails = tokenProvider.getUserIdFromJWT(jwt);
		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, userDetails, userDetails.getAuthorities()));
	}
	

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity(new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"), HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(), signUpRequest.getPassword());

		user.setPassword(passwordEncoder.encode(user.getPassword()));

		Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
				.orElseThrow(() -> new AppException("User Role not set."));

		user.setRoles(Collections.singleton(userRole));

		User result = userRepository.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(result.getUsername()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
	}
	
	@GetMapping(path = {"/{id}"})
	public User authenticateUser(@PathVariable("id") String id) {
		System.out.println("iiiiii" +id);
		User user = new User();
		if ( userRepository.existsById(Long.parseLong(id))) {
			user =  userRepository.findById(Long.parseLong(id)).orElseThrow(() -> new AppException("User not founded."));
		}
		return user;
		//return ResponseEntity.ok("okk"+user);
	}
	
	@PostMapping(path = "/addRole", consumes = "application/json", produces = "application/json")
	public String addRole(@RequestBody RoleRequest roleRequest) {
		Role role = new Role();
		Role role2 = new Role();
		role.setName(RoleName.ROLE_USER);
		roleRepository.save(role);
		role2.setName(RoleName.ROLE_ADMIN);
		roleRepository.save(role2);
		return "added successfully";
	}
	
	
	
	
	
}
